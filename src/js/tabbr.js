class Tabbr {
    constructor(id, options = {}) {
        this.element = document.getElementById(id);
        this.tabs = this.element.children;

        const defaults = {
            maxHeight: null,
            minHeight: null
        };
        this.setup = Object.assign({}, defaults, options);

        this.setupTabs();
        this.setupNavigation();
        this.initialize();
    }

    setupTabs() {
        var container = document.createElement("div");
        container.classList.add("tabbr__tabs");


        var i = 0;
        while (this.tabs[0] != null) {
            container.appendChild(this.tabs[0]);
        }
        this.element.innerHTML = '';
        this.element.appendChild(container);

        this.container = this.element.getElementsByClassName('tabbr__tabs')[0];
        this.tabs = this.container.children;

        for (i = 0; i < this.tabs.length; i++) {
            this.tabs[i].classList.add("tabbr__tab");
        }

        if (this.setup.minHeight != null) {
            this.container.style.minHeight = this.setup.minHeight + "px";
        }

        if(this.setup.maxHeight != null) {
            this.container.style.maxHeight = this.setup.maxHeight + "px";
        }
    }

    setupNavigation() {
        var navigation = document.createElement('div');
        var tabs = this.container.children;
        navigation.classList.add("tabbr__nav");
        var i = 0;
        for (i = 0; i < tabs.length; i++) {
            var item = document.createElement('button');
            if (tabs[i].dataset.title != null) {
                item.innerHTML = tabs[i].dataset.title;
            }
            else {
                item.innerHTML = "Tab " + i;
            }
            item.setAttribute("href", "#");
            item.classList.add("tabbr__nav-item");
            item.setAttribute("data-tab", i);

            var self = this;
            item.onclick = function () {
                self.switchToTab(this.dataset.tab);
            }

            navigation.appendChild(item);
        }
        this.element.insertBefore(navigation, this.container);
        this.navigation = navigation;
    }

    initialize() {
        var i = 0;
        var activeTabIndex = this.getActiveTabIndex();

        if (activeTabIndex != null) {
            for (i = 0; i < this.tabs.length; i++) {
                if (i != activeTabIndex) {
                    this.tabs[i].style.display = "none";
                }
                this.tabs[activeTabIndex].style.display = "block";
                this.navigation.children[activeTabIndex].classList.add("active");
            }

        }
        else {
            this.tabs[0].classList.add("active");
            this.navigation.children[0].classList.add("active");
            for (i = 0; i < this.tabs.length; i++) {
                if (i != 0) {
                    this.tabs[i].style.display = "none";
                }
            }
        }
    }

    getActiveTabIndex() {
        var i = 0;
        for (i = 0; i < this.tabs.length; i++) {
            var tab = this.tabs[i];
            if (tab.classList.contains("active")) {
                return i;
            }
        }
        return null;
    }

    switchToTab(tabIndex) {
        var current = this.getActiveTabIndex();
        if (tabIndex != current) {
            this.tabs[tabIndex].classList.add("active");
            this.tabs[current].classList.remove("active");
            this.tabs[current].style.display = "none";
            this.tabs[tabIndex].style.display = "block";
            this.navigation.children[current].classList.remove("active");
            this.navigation.children[tabIndex].classList.add("active");
        }
    }
}